Our project consists of several parts:
1. to see the experiment result, view 'Results.ipynb'
2. to generate CoVe, see the readme file in directory 'Context_Vectors'
3. to do Sentiment Classification, go to directory 'Classification' and run main.py
4. to do QuestionAnswer, see the readme file in directory 'QuestionAnswer'