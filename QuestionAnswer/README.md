## Question Answering Model

this file implements the *biattentive classification network*

to run the model,  follow up the below steps:

1. move into the preprocessing directory and run the download_preprocess.py to download the data. the data will then present in the /data directory

2. move the CoVe.h5 file into the /data directory for loading CoVe model

3. to do question answering, run main.py with --arguments for <mode>, <model name>. see detailed information in the main.py  