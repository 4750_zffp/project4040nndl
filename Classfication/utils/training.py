# helper function to facilitate model training

import tensorflow as tf
import numpy as np

from timeit import default_timer as timer
from sklearn.metrics import f1_score, accuracy_score
from collections import Counter


_PAD = "<pad>"
_SOS = "<sos>"
_UNK = "<unk>"

PAD_ID = 0
SOS_ID = 1
UNK_ID = 2

def f1(prediction, truth):
    return f1_score(prediction, truth, average = 'macro')

def accuracy(prediction, truth):
    return accuracy_score(prediction, truth)


def reverse_indices(indices, rev_vocab):
    """ Recovers words from embedding indices

    Args:
        indices: Integer indices to recover words for.
        rev_vocab: Reverse vocabulary. Dictionary mapping indices to words.

    Returns:
        String of words with space as separation
    """
    return ' '.join([rev_vocab[idx] for idx in indices if idx != PAD_ID])

def multibatch_prediction_truth(session, model, data, FLAGS, num_batches=None, random=False):
    """ Returns batches of predictions and ground truth answers.

    Args:
        session: TensorFlow Session.
        model: classification model 
        FLAGS: configuration parameters
    Returns:
        Tuple of Predictions,Truth
    """
    if num_batches is None:
        num_batches = data.length // FLAGS.batch_size
    truth = []
    predict = []

    for i in range(num_batches):
        if random:
            input_text, input_length, true_label = data.get_batch(FLAGS.batch_size)
        else:
            begin_idx = i * FLAGS.batch_size
            input_text, input_length, true_label = data[begin_idx:begin_idx + FLAGS.batch_size]
        predicted_label = session.run(model.prediction, model.fill_feed_dict(input_text, input_length,true_label))
        # append the answer
        predict.append(predicted_label)
        truth.append(true_label)

    predict = np.concatenate(predict)
    truth = np.concatenate(truth)

    return predict, truth