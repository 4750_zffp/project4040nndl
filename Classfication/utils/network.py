# helper functions for potential network usage

import tensorflow as tf
from keras.models import load_model

#-------------------------------------------BidirectionalAttentionNetwork-------------------------------------------

def maybe_dropout(keep_prob, is_training):
    return tf.cond(tf.convert_to_tensor(is_training), lambda: keep_prob, lambda: 1.0)