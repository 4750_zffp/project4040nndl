# helper functions for data process

import os
from os.path import join as pjoin
import tensorflow as tf

def get_data_paths(data_dir, name='train'):
    text_file = pjoin(data_dir, '{}.ids.text'.format(name))
    label_file = pjoin(data_dir, '{}.label'.format(name))
    return text_file, label_file




