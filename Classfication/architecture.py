# Network Structure
"""
This class implements the Biattentive Classification Network model described in section 5 of
`Learned in Translation: Contextualized Word Vectors (NIPS 2017)
<https://arxiv.org/abs/1708.00107>`_ for text classification.
Given a piece of text, it predict output label.

At a high level, the model starts by embedding the tokens and running them through
a feed-forward neural net

Then, we encode these representations with a encoder.
run bi-attention on the encoder output represenatations and
get out an attentive vector representation of the text.

combine this text representation with the encoder outputs computed earlier,
and then run this through yet another. [integrator]

Lastly, we passed through a maxout network or some feed-forward layers
to output a classification. [Maxout]
"""
import tensorflow as tf
from utils.network import *


# fundamental RNN cell
def RNN_cell(state_size, is_training, input_keep_prob=1.0, output_keep_prob=1.0, state_keep_prob=1.0, activation = tf.tanh):
    # lstm cell unit
    cell = tf.contrib.rnn.LSTMCell(num_units=state_size, activation=activation)
    input_keep_prob = maybe_dropout(input_keep_prob, is_training)
    output_keep_prob = maybe_dropout(output_keep_prob, is_training)
    state_keep_prob = maybe_dropout(state_keep_prob, is_training)
    dropout_cell = tf.contrib.rnn.DropoutWrapper(
        cell,
        input_keep_prob=input_keep_prob,
        output_keep_prob=output_keep_prob,
        state_keep_prob=state_keep_prob
    )
    return dropout_cell

def lstm_encode(cell, input):
    '''
    encode the input with -> LSTM
    Args:
        cell: specified cells
        input: Tensor of rank 3, shape [N, S, ?]
        input_len: S
    Return:
        concatednated lstm_encoding [N, D, 2H]
    '''
    lstm_fw_bw_encodings, _ = tf.nn.bidirectional_dynamic_rnn(
        cell_fw = cell,
        cell_bw = cell,
        dtype = tf.float32,
        inputs = input
    )
    # concatenate both the forward and backward
    lstm_encoding = tf.concat(lstm_fw_bw_encodings, 2)
    return lstm_encoding

def CoVe_encode(input_embedding, CoVe_model):
    '''
    encode the input with #CoVe
    apply loaded pre-trained CoVe model
    concatenate the CoVE encoding with the original input embedding

    Args:
        input_embedding: shape of [batch_size, num_sequence, GloVe_dim]
        CoVe_model: pre-trained CoVe model .h5 file
    Return:
        Encoded Matrix
    '''
    # -> CoVe Encodedding
    input_cove = CoVe_model(input_embedding)
    # concatenate the original embedding as well as the CoVe encoded component
    concat_info = tf.concat([input_embedding, input_cove], 2)

    return concat_info


def bi_attention(x, y, W_s, keep_prob = 0.8):
    """
    Bi Attention Mechanism
    Args:
        x: tensor of shape [batch,N,h]
        y: x: tensor of shape [batch,M,h]
        W_s: trainable weight to control the flow of attention memory
        keep_prob: probability to keep
    Return:
        shape (batch_size, N, h * 3)
    """
    ## -> Similarity Matrix
    # pointwise multiplication
    x_expand = tf.expand_dims(x, 2)  # [batch,N,1,h]
    y_expand = tf.expand_dims(y, 1)  # [batch,1,M,h]

    xy_pointWise = x_expand * y_expand  # [batch,N,M,h]

    x_input = tf.tile(x_expand, [1, 1, tf.shape(x)[1], 1])
    y_input = tf.tile(y_expand, [1, tf.shape(y)[1], 1, 1])

    ## -> concatenation input
    concat_input = tf.concat([x_input, y_input, xy_pointWise], -1)  # [batch,N,M,3h]
    # derive the matrix
    similarity = tf.reduce_sum(concat_input * W_s, axis = 3)  # [batch,N,M]

    ## -> Attention Weights
    # extract context to question attention
    # take softmax over y
    attention_y_weight = tf.nn.softmax(similarity, 2)  # shape (batch, N).
    # attention distribution to take weighted sum of values
    x2y = tf.matmul(attention_y_weight, y)  # shape (batch, N, h)

    # Calculating question to context attention c_dash
    S_max = tf.reduce_max(similarity, axis = 2)  # shape (batch, N)
    x_dash_dist = tf.nn.softmax(S_max, 1)  # distribution of shape (batch, N)
    attention_x_weight = tf.expand_dims(x_dash_dist, 1)  # shape (batch, 1, N)
    x_dash = tf.matmul(attention_x_weight, x)  # shape (batch_size, 1, h)

    # element-wise multiplying with x to get the matrix
    x_x2y = x * x2y  # shape (batch, N, h)
    x_x_dash = x * x_dash  # shape (batch, N, h)

    # concatenate the output
    output = tf.concat([x2y, x_x2y, x_x_dash], axis = 2)  # (batch_size, N, h * 3)

    # Apply dropout
    output = tf.nn.dropout(output, keep_prob)

    return output

def maxout_network(inputs, hidden_size, class_num,  keep_prob=0.8):
    """ Highway maxout network.

    Args:
        inputs: Tensor of rank 3, shape [N, D, ?]. Inputs to network.
        hidden_size: Scalar integer. Hidden units of highway maxout network.
        pool_size: Scalar integer. Number of units that are max pooled in maxout layer.
        keep_prob: Scalar float. Input dropout keep probability for maxout layers.
    Return:
        Tensor of rank 2, shape [N, D]. Logits.
    """
    inputs = tf.nn.dropout(inputs, keep_prob)
    layer1 = tf.layers.dense(inputs, hidden_size)

    output = tf.contrib.layers.maxout(layer1 , class_num)
    print('output', output)
    return output
