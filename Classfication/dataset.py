# Dataset Pipeline to process and feed the traning data
# implement DataPipeline class
#           fundamental function: get_batch
#                                 _read_into_memory

import numpy as np
from utils.training import PAD_ID

class DataPipeline:
    def __init__(self, input_file, label_file, max_word_length):
        # file path of data file {train|val|dev}
        self.input_file = input_file
        self.label_file = label_file
        self.max_word_length = max_word_length
        self.length = -1

        # read files into memory to save I/O cost
        # also checks that lengths match
        self._read_into_memory()
        self.epoch_sampled = 0
        self.epoch = 1

    @staticmethod
    def read_file(file):
        def process_line(line):
            data = line.strip().split(' ')
            data = [int(pt) for pt in data]
            return data

        with open(file) as f:
            dataset = [process_line(line) for line in f]
        return dataset

    def _read_into_memory(self):
        '''
        save data into memory
        '''
        text = self.read_file(self.input_file)
        label = self.read_file(self.label_file)

        # remove all texts that are cut by its max length
        self.input_text, self.true_label = zip(*
            [
                (t, l[0])
                for t, l in zip(text, label)
                if len(t) < self.max_word_length
            ]
        )
        assert len(self.input_text) == len(self.true_label)
        self.length = len(self.input_text)

    def get_batch(self, batch_size, replace=True):
        '''

        :param batch_size:
        :param replace:
        :return:
        '''
        if replace:
            batch_idx = np.random.choice(self.length, batch_size, replace)
            return self[batch_idx]

        if self.epoch_sampled == 0 and self.epoch == 1:
            self.shuffle()

        if self.epoch_sampled == self.length:
            self.shuffle()
            self.epoch_sampled = 0
            self.epoch += 1
        
        tail_batch = []
        if self.epoch_sampled + batch_size > self.length:
            tail_batch = self[self.epoch_sampled:]
            batch_size -= len(tail_batch[0])
            self.shuffle()
            self.epoch_sampled = 0
            self.epoch += 1
        
        batch = self[self.epoch_sampled:self.epoch_sampled+batch_size]
        self.epoch_sampled += batch_size
        if tail_batch:
            batch = self.join(tail_batch, batch)
        return batch


    def shuffle(self):
        """ https://stackoverflow.com/questions/11765061/better-way-to-shuffle-two-related-lists """
        from random import shuffle
        text_shuf = []
        label_shuf = []
        index_shuf = list(range(self.length))
        shuffle(index_shuf)

        for i in index_shuf:
            text_shuf.append(self.input_text[i])
            label_shuf.append(self.true_label[i])

        self.input_text, self.true_label = tuple(text_shuf), tuple(label_shuf)


    @staticmethod
    def join(first, second):
        a = []
        for i, j in zip(first, second):
            a.append(i+j)
        return tuple(a)


    def __getitem__(self, arg):
        from collections.abc import Iterable

        if isinstance(arg, int):
            arg = [arg]

        if isinstance(arg, Iterable):
            input_text, input_length = pad_sequences([self.input_text[i] for i in arg], self.max_word_length)
            true_label = [self.true_label[i] for i in arg]

        if isinstance(arg, slice):
            input_text, input_length = pad_sequences(self.input_text[arg], self.max_word_length)
            true_label = self.true_label[arg]

        return (input_text, input_length, true_label)


def pad_sequence(sequence, max_length):
        """ Pads data of format `(sequence, labels)` to `max_length` sequence length
        and returns a triplet `(sequence_, labels_, mask)`. If
        the length of the sequence is longer than `max_length` then it 
        is truncated to `max_length`.
        """

        # create padding vectors
        sequence_padding = PAD_ID
        
        pad_length = max([0, max_length - len(sequence)])
        padded_sequence = sequence[:max_length]
        padded_sequence.extend([sequence_padding]*pad_length)
        length = min([len(sequence), max_length])

        return padded_sequence, length


def pad_sequences(sequences, max_length):
    padded_sequences, lengths = zip(*[pad_sequence(sequence, max_length) for sequence in sequences])
    return padded_sequences, lengths

