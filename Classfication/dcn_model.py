"""
BiattentiveClassificationNetwork

Encoder:
    - feedforward network with ReLU activation [Nair and Hinton, 2010]
    - bidirectional LSTM 

    - Biattention mechanism

    - Integrate

    - Pooled Representation
    
    - Maxout Network
"""

import copy
import tensorflow as tf
from architecture import RNN_cell, dcn_encode, dcn_decode, dcn_loss
from tensorflow.contrib.seq2seq.python.ops.attention_wrapper import _maybe_mask_score
from utils.network import *

class BiattentiveClassificationNetwork:
    """ 
    Builds graph for BCN-type models  
    
    Args:  
        pretrained_embeddings: Pretrained embeddings.  
        hparams: dictionary of all hyperparameters for models.  
    """
    def __init__(self, pretrained_embeddings, hparams):
        self.hparams = copy.copy(hparams)
        self.pretrained_embeddings = pretrained_embeddings

        ## set up Placeholders
        self.X = tf.placeholder(tf.int32, (None, None), name='question')
        self.X_length = tf.placeholder(tf.int32, (None,), name='question_length')
        self.Y = tf.placeholder(tf.int32, (None, None), name='paragraph')
        self.Y_length = tf.placeholder(tf.int32, (None,), name='paragraph_length')
        self.answer_span = tf.placeholder(tf.int32, (None, 2), name='answer_span')
        self.is_training = tf.placeholder(tf.bool, shape=(), name='is_training')

        ## Word Embeddings
        with tf.variable_scope('Embeddings'):
            embedded_vocab = tf.Variable(self.pretrained_embeddings, name='word_embedding', trainable=hparams['trainable_embeddings'], dtype=tf.float32)  
            X_embeddings = tf.nn.embedding_lookup(embedded_vocab, self.X)
            Y_embeddings = tf.nn.embedding_lookup(embedded_vocab, self.Y)

        # Set up RNN Cells
        cell = lambda: RNN_cell(hparams['cell'], hparams['state_size'], self.is_training, hparams['input_keep_prob'], hparams['output_keep_prob'], hparams['state_keep_prob'])
        final_cell = lambda: RNN_cell(hparams['cell'], hparams['state_size'], self.is_training, hparams['final_input_keep_prob'], hparams['output_keep_prob'], hparams['state_keep_prob'])  # TODO TEMP

        with tf.variable_scope('ReLu_Feedforward'):
            with tf.variable_scope('initial_encoder'):
        initial = cell_factory()
        query_encoding, document_encoding = query_document_encoder(initial, query, query_length, document, document_length)
        query_encoding = tf.layers.dense(
            query_encoding, 
            query_encoding.get_shape()[2], 
            activation=tf.tanh,
        )


        ## Encoders
        with tf.variable_scope('Prediction'):
            # @DCN Encoding
            encoding = dcn_encode(cell, final_cell, q_embeddings, self.question_length, p_embeddings, self.paragraph_length)
            encoding = tf.nn.dropout(encoding, keep_prob=maybe_dropout(hparams['encoding_keep_prob'], self.is_training))
        
        ## Preidction
        with tf.variable_scope('Prediction'):
            # Dynamic Points Decoder
            logits = dcn_decode(encoding, self.paragraph_length, hparams['state_size'], hparams['pool_size'], hparams['max_iter'], keep_prob=maybe_dropout(hparams['keep_prob'], self.is_training))
            last_iter_logit = logits.read(hparams['max_iter']-1)
            start_logit, end_logit = last_iter_logit[:,:,0], last_iter_logit[:,:,1]
            start = tf.argmax(start_logit, axis=1, name='answer_start')
            if hparams['force_end_gt_start']:
                end_logit = maybe_mask_to_start(end_logit, start, -1e30)
            if hparams['max_answer_length'] > 0:
                end_logit = _maybe_mask_score(end_logit, start+hparams['max_answer_length'], -1e30)
            self.answer = (start, tf.argmax(end_logit, axis=1, name='answer_end'))

        ## Loss
        with tf.variable_scope('Loss'):
            self.loss = dcn_loss(logits, self.answer_span, max_iter=hparams['max_iter'])

        # last iterative loss
        with tf.variable_scope('last_iter_loss'):
            start_loss = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=start_logit, labels=self.answer_span[:, 0], name='start_loss')
            end_loss = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=end_logit, labels=self.answer_span[:, 1], name='end_loss')
            last_loss = tf.reduce_mean(start_loss + end_loss)
        tf.summary.scalar('cross_entropy_last_iter', last_loss)

        global_step = tf.train.get_or_create_global_step()
        with tf.variable_scope('Train'):
            if hparams['exponential_decay']:
                lr = tf.train.exponential_decay(learning_rate=hparams['learning_rate'],
                                            global_step=global_step,
                                            decay_steps=hparams['decay_steps'],
                                            decay_rate=hparams['decay_rate'],
                                            staircase=hparams['staircase'])
            else:
                lr = hparams['learning_rate']
            optimizer = tf.train.AdamOptimizer(lr)
            grad, tvars = zip(*optimizer.compute_gradients(self.loss))
            if hparams['clip_gradients']:
                grad, _ = tf.clip_by_global_norm(grad, hparams['max_gradient_norm'], name='gradient_clipper')
            grad_norm = tf.global_norm(grad)
            self.train = optimizer.apply_gradients(zip(grad, tvars), global_step=global_step, name='apply_grads')
        
        tf.summary.scalar('cross_entropy', self.loss)
        tf.summary.scalar('learning_rate', lr)
        tf.summary.scalar('grad_norm', grad_norm)
    

    def fill_feed_dict(self, question, paragraph, question_length, paragraph_length, answer_span=None, is_training=False):
        feed_dict = {
            self.question: question,
            self.paragraph: paragraph,
            self.question_length: question_length, 
            self.paragraph_length: paragraph_length,
            self.is_training: is_training
        }

        if answer_span is not None:
            feed_dict[self.answer_span] = answer_span

        return feed_dict