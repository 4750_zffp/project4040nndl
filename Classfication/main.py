# Main Function to start the model
import json
import logging
import itertools
from datetime import datetime

import tensorflow as tf
import numpy as np

from preprocessing.download_preprocess import tokenize,UNK_ID, PAD_ID
from utils.datatool import *
from utils.training import *

from bcn_model import BiattentiveClassificationNetwork
from dataset import DataPipeline

logging.basicConfig(level=logging.INFO)

tf.app.flags.DEFINE_string('f', '', 'kernel')


tf.app.flags.DEFINE_boolean("is_CoVe", False, "implement CoVe encoding layer or not")
tf.app.flags.DEFINE_boolean("is_maxout", True, "Whether to use exponential decay.")
tf.app.flags.DEFINE_string("model_name", 'SST2-BCN-Normal-'+ datetime.now().strftime('hour%h'), "Models name, used for folder management.")

## Model hyperparameters
tf.app.flags.DEFINE_integer("embedding_size", 300, "Size of the pretrained vocabulary.")
tf.app.flags.DEFINE_integer("state_size", 150, "Size of each model layer.")
tf.app.flags.DEFINE_integer("class_number", 2,  "Total number of the classification type")
tf.app.flags.DEFINE_boolean("trainable_embeddings", False, "Make embeddings trainable.")
tf.app.flags.DEFINE_float("input_keep_prob", 0.8, "Encoder: Fraction of units randomly kept of inputs to RNN.")
tf.app.flags.DEFINE_float("output_keep_prob", 0.8, "Encoder: Fraction of units randomly kept of outputs from RNN.")
tf.app.flags.DEFINE_float("state_keep_prob", 0.8, "Encoder: Fraction of units randomly kept of encoder states in RNN.")
tf.app.flags.DEFINE_float("encoding_keep_prob", 0.8, "Encoder: Fraction of encoding output kept.")
tf.app.flags.DEFINE_float("final_input_keep_prob", 0.8, "Encoder: Fraction of units randomly kept of inputs to final encoder RNN.")


# Mode
tf.app.flags.DEFINE_string('mode', 'train', 'Mode to use, train/eval/shell/overfit')
# Training hyperparameters
tf.app.flags.DEFINE_integer("max_steps", 15000, "Steps until training loop stops.")
tf.app.flags.DEFINE_float("learning_rate", 0.005, "Learning rate.")


## Data hyperparameters
tf.app.flags.DEFINE_integer("max_sequence_length", 35, "Maximum review length.")
tf.app.flags.DEFINE_integer("batch_size", 32, "Batch size to use during training.")
# exponential decay
tf.app.flags.DEFINE_boolean("exponential_decay", True, "Whether to use exponential decay.")
tf.app.flags.DEFINE_float("decay_steps", 4000, "Number of steps for learning rate to decay by decay_rate")
tf.app.flags.DEFINE_boolean("staircase", True, "Whether staircase decay (use of integer division in decay).")
tf.app.flags.DEFINE_float("decay_rate", 0.75, "Learning rate.")
# gradient clipping
tf.app.flags.DEFINE_boolean("clip_gradients", True, "Whether to clip gradients.")
tf.app.flags.DEFINE_float("max_gradient_norm", 10.0, "Clip gradients to this norm.")

# Evaluation arguments
tf.app.flags.DEFINE_integer("eval_batches", 80, "Number of batches of size batch_size to use for evaluation.")

# Print
tf.app.flags.DEFINE_integer("global_steps_per_timing", 500, "Number of steps per global step per sec evaluation.")
tf.app.flags.DEFINE_integer("print_every", 200, "How many iterations to do per print.")

# Directories etc.
tf.app.flags.DEFINE_string("data_dir", os.path.join(os.curdir, "data"), "SST directory (default ./data)")
tf.app.flags.DEFINE_string("train_dir", os.path.join(os.curdir, "train"), "Training directory to save the model parameters (default: ../checkpoints).")
tf.app.flags.DEFINE_string("CoVe_dir", os.path.join(os.curdir, "data", "CoVe.h5"), "CoVe model directory")
tf.app.flags.DEFINE_string("vocab_path", os.path.join(os.curdir, "data", "vocab.dat"), "Path to vocab file (default: ../data/squad/vocab.dat)")

FLAGS = tf.app.flags.FLAGS

def parameter_space_size():
    """ Parameter space size information """
    num_parameters = sum(v.get_shape().num_elements() for v in tf.trainable_variables())
    logging.info('Number of parameters %d' % num_parameters)
    for v in tf.trainable_variables():
        logging.info('Variable {} has {} entries'.format(v, v.get_shape().num_elements()))


def do_eval(model, train, dev):
    """ Evaluates a model on training and development set

    Args:  
        model: QA model that has an instance variable 'answer' that returns answer span and takes placeholders  
        question, question_length, paragraph, paragraph_length  
        train: Training set  
        dev: Development set
    """
    checkpoint_dir = os.path.join(FLAGS.train_dir, FLAGS.model_name)
    parameter_space_size()
    saver = tf.train.Saver()
    # Training session
    with tf.Session() as session:
        saver.restore(session, tf.train.latest_checkpoint(checkpoint_dir))
        print('Evaluation in progress.', flush=True)

        # Train/Dev Evaluation
        start_evaluate = timer()

        # derive the prediction
        prediction, truth = multibatch_prediction_truth(session, model, train,  FLAGS, num_batches=FLAGS.eval_batches)
        train_f1 = f1(prediction, truth)
        train_acc = accuracy(prediction, truth)

        prediction, truth = multibatch_prediction_truth(session, model, dev, FLAGS, num_batches=FLAGS.eval_batches)
        dev_f1 = f1(prediction, truth)
        dev_acc = accuracy(prediction, truth)

        logging.info('Train/Dev F1: {:.3f}/{:.3f}'.format(train_f1, dev_f1))
        logging.info('Train/Dev Accuracy: {:.3f}/{:.3f}'.format(train_acc, dev_acc))
        logging.info('Time to evaluate: {:.1f} sec'.format(timer() - start_evaluate))


def do_train(model, train, dev, save_step = 1000):
    """ Trains a model

    Args:  
        model: QA model that has an instance variable 'answer' that returns answer span and takes placeholders  
        question, question_length, paragraph, paragraph_length  
        train: Training set  
        dev: Development set
    """
    parameter_space_size()

    checkpoint_dir = os.path.join(FLAGS.train_dir, FLAGS.model_name)
    summary_writer = tf.summary.FileWriter(checkpoint_dir)

    losses = []
    init = tf.global_variables_initializer()
    summary = tf.summary.merge_all()
    saver = tf.train.Saver()

    # Training session  
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    with tf.Session(config=config) as sess:
        sess.run(init)
        latest_ckpt = tf.train.latest_checkpoint(checkpoint_dir)
        if latest_ckpt:
            saver.restore(sess, latest_ckpt)
        start = timer()
        epoch = -1
        for i in itertools.count():
            feed_dict = model.fill_feed_dict(*train.get_batch(FLAGS.batch_size, replace=False), is_training=True)
            if epoch != train.epoch:
                epoch = train.epoch
                print('Epoch {}'.format(epoch))

            fetch_dict = {
                'step': tf.train.get_global_step(),
                'loss': model.loss,
                'train': model.train
            }
            if i > 0 and (step+1) % 20 == 0:
                fetch_dict['summary'] = summary
            result = sess.run(fetch_dict, feed_dict)
            step = result['step']
            if 'summary' in result:
                summary_writer.add_summary(result['summary'], step)

            # save the model at each 1000 step
            if step > 0 and (step==50 or (step % save_step == 0)):
                saver.save(sess, os.path.join(checkpoint_dir, 'model'), step)

            # Moving Average loss
            losses.append(result['loss'])
            if step == 1 or step == 10 or step == 50 or step == 100 or step % FLAGS.print_every == 0:
                mean_loss = sum(losses)/len(losses)
                losses = []
                print('Step {}, loss {:.2f}'.format(step, mean_loss))

            # Train/Develop Evaluation
            if step == 1 or step == 100 or step % 100 == 0:
                feed_dict = model.fill_feed_dict(*dev.get_batch(FLAGS.batch_size))
                fetch_dict = {'loss': model.loss}
                dev_loss = sess.run(fetch_dict, feed_dict)['loss']
                start_evaluate = timer()

                # dev
                prediction, truth = multibatch_prediction_truth(sess, model, dev, FLAGS, num_batches=40, random=True)
                dev_f1 = f1(prediction, truth)
                dev_acc = accuracy(prediction, truth)

                #
                prediction, truth = multibatch_prediction_truth(sess, model, train, FLAGS, num_batches=40, random=True)
                # performance metrics
                train_acc = accuracy(prediction, truth)
                train_f1 = f1(prediction, truth)

                # record the F1 Score
                summary_writer.add_summary(tf.Summary(value=[tf.Summary.Value(tag='F1_develop', simple_value=dev_f1)]), step)
                summary_writer.add_summary(tf.Summary(value=[tf.Summary.Value(tag='F1_train', simple_value=train_f1)]), step)
                # record the Accuracy
                summary_writer.add_summary(tf.Summary(value = [tf.Summary.Value(tag = 'Accuracy_develop', simple_value = dev_acc)]), step)
                summary_writer.add_summary(tf.Summary(value = [tf.Summary.Value(tag = 'Accuracy_train', simple_value = train_acc)]), step)

                print('Step {}, Dev loss {:.2f}, Train/Dev F1: {:.3f}/{:.3f},'
                      'Train/Dev Acc: {:.3f}/{:.3f},\n'
                      'Time to evaluate: {:.1f} sec'.format(step, dev_loss,train_f1, dev_f1, train_acc, dev_acc, timer() - start_evaluate))
            
            if step > 0 and step % FLAGS.global_steps_per_timing == 0:
                time_iter = timer() - start
                print('INFO:global_step/sec: {:.2f}/{:.2f}'.format(FLAGS.global_steps_per_timing,time_iter ))
                start = timer()
            
            if step == FLAGS.max_steps:
                break

def save_flags():
    """ Saves flags in checkpoints folder without overwriting previous """
    model_path = os.path.join(FLAGS.train_dir, FLAGS.model_name)
    if not os.path.exists(model_path):
        os.makedirs(model_path)
    
    for i in itertools.count():
        json_path = os.path.join(FLAGS.train_dir, FLAGS.model_name, "flags_{}.json".format(i))
        if os.path.exists(json_path):
            with open(json_path, 'r') as f:
                if json.load(f) == FLAGS.flag_values_dict():
                    break
        else:
            with open(json_path, 'w') as f:
                json.dump(FLAGS.flag_values_dict(), f, indent=4)
            break


def main(_):
    """ Typical usage

    For <model_name> see your folder name in ../checkpoints. 

    Training
    ``` sh
    $ python main.py --mode train --model <model> (if restoring or naming a model: --model_name <model_name>)
    ```
    
    Evaluation
    ``` sh
    $ python main.py --mode eval --model <model> --model_name <model_name>
    ```

    Shell
    ``` sh
    $ python main.py --mode shell --model <model> --model_name <model_name>
    ```
    """
    # Load data
    train_dt = DataPipeline(*get_data_paths(FLAGS.data_dir, name='train'), FLAGS.max_sequence_length)

    dev_dt = DataPipeline(*get_data_paths(FLAGS.data_dir, name='val'), FLAGS.max_sequence_length)

    logging.info('Train/Dev Size {}/{}'.format(train_dt.length, dev_dt.length))

    # Load embeddings
    embed_path = pjoin(FLAGS.data_dir, "glove.trimmed.{}.npz".format(FLAGS.embedding_size))
    embeddings = np.load(embed_path)['glove']
    
    # Build up model
    clf_model = BiattentiveClassificationNetwork(embeddings, FLAGS.flag_values_dict())

    # Run Mode
    if FLAGS.mode == 'train':
        save_flags()
        do_train(clf_model, train_dt, dev_dt)
    elif FLAGS.mode == 'eval':
        do_eval(clf_model, train_dt, dev_dt)
    elif FLAGS.mode == 'overfit':
        test_overfit(clf_model, train_dt, FLAGS)
    else:
        raise ValueError('Incorrect mode entered, {}'.format(FLAGS.mode))

if __name__ == "__main__":
    tf.app.run()

