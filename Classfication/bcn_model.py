"""
Biattentive-Classification-Network

TensorFlow implementation
"""

import copy

from architecture import RNN_cell, lstm_encode, CoVe_encode, bi_attention, maxout_network
from utils.network import *

class BiattentiveClassificationNetwork:
    """ Builds graph for BCN-type models
    
    Structure:
    - feedforward network with ReLU activation [Nair and Hinton, 2010]
    - bidirectional LSTM
    - Biattention mechanism
    - Integrate
    - Maxout Network

    Args:  
        pretrained_embeddings: Pretrained embeddings.  
        hparams: dictionary of all hyperparameters for models.  
    """
    def __init__(self, pretrained_embeddings, hparams):
        self.hparams = copy.copy(hparams)
        self.pretrained_embeddings = pretrained_embeddings

        ## set up Placeholders
        self.input_word = tf.placeholder(tf.int32, (None, None), name='input_word_sequence')
        self.seq_length = tf.placeholder(tf.int32, (None,), name='sequence_length')

        self.true_label = tf.placeholder(tf.int32, (None,), name='true_label')
        self.label_hot = tf.one_hot(self.true_label, depth=hparams['class_number'])
        self.is_training = tf.placeholder(tf.bool, shape=(), name='is_training')

        ## Word Embeddings
        with tf.variable_scope('Embeddings'):
            embedded_vocab = tf.Variable(self.pretrained_embeddings, name='shared_embedding', trainable=hparams['trainable_embeddings'], dtype=tf.float32)  
            input_embeddings = tf.nn.embedding_lookup(embedded_vocab, self.input_word)

        ## @ CoVe Structure Design
        if hparams['is_CoVe']:
            # load the CoVe model
            print('loading the CoVE model...')
            CoVe_model = load_model(hparams['CoVe_dir'])
            with tf.variable_scope('CoVe_Encoder'):
                # introduce our CoVe Encoding
                input_embeddings = CoVe_encode(input_embeddings, CoVe_model)

        with tf.variable_scope('ReLu_Feedforward'):
            relu_cell = lambda: RNN_cell(hparams['state_size'], self.is_training,
                                         input_keep_prob =hparams['input_keep_prob'], output_keep_prob=hparams['output_keep_prob'],
                                         state_keep_prob =hparams['state_keep_prob'], activation = tf.nn.relu)
            # encoded with Bi-LSTM layer
            input_encoding = lstm_encode(relu_cell(), input_embeddings)

        ## BiAttention Mechanism
        with tf.variable_scope('BiAttention'):# attention weight

            self.W_s = tf.get_variable('W_s', [hparams['state_size'] * 6], tf.float32,
                                        tf.contrib.layers.xavier_initializer())

            # for classfication case, inputs are the same
            attention_output = bi_attention(input_encoding, input_encoding, self.W_s)

        ## Integration
        with tf.variable_scope('Integration'):
            # introduce LSTM to capture the dependence of attention
            integrate_cell = lambda: RNN_cell(hparams['state_size'], self.is_training, hparams['final_input_keep_prob'],
                                              hparams['output_keep_prob'], hparams['state_keep_prob'])
            integrate_output = lstm_encode(integrate_cell(), attention_output)[:, -1, :]
            print('integrate', integrate_output)


        ## Maxout
        with tf.variable_scope('Maxout'):
            # batch-normalized
            batch_norm_output = tf.layers.batch_normalization(integrate_output)

            # three-layer
            if hparams['is_maxout']:
                output_logits = maxout_network(batch_norm_output, hparams['state_size'], hparams['class_number'], keep_prob= hparams['state_keep_prob'])
            else:
                output_logits = tf.layers.dense(batch_norm_output, hparams['class_number'])
            output_logits = tf.nn.softmax(output_logits, -1)
            print('output', output_logits)

        ## Prediction
        with tf.variable_scope('Prediction'):
            self.prediction = tf.argmax(output_logits, 1)

        ## Loss
        with tf.variable_scope('Loss'):
            batch_loss = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=self.true_label, logits=output_logits)
            self.loss = tf.reduce_mean(batch_loss)

        tf.summary.scalar('loss', self.loss)

        # global step to keep track of training step
        global_step = tf.train.get_or_create_global_step()
        with tf.variable_scope('Train'):
            if hparams['exponential_decay']:
                lr = tf.train.exponential_decay(learning_rate=hparams['learning_rate'],
                                            global_step=global_step,
                                            decay_steps=hparams['decay_steps'],
                                            decay_rate=hparams['decay_rate'],
                                            staircase=hparams['staircase'])
            else:
                lr = hparams['learning_rate']

            # Optimization
            optimizer = tf.train.AdamOptimizer(lr)
            grad, tvars = zip(*optimizer.compute_gradients(self.loss))

            ## Gradient Clipping
            if hparams['clip_gradients']:
                grad, _ = tf.clip_by_global_norm(grad, hparams['max_gradient_norm'], name='gradient_clipper')
            grad_norm = tf.global_norm(grad)
            self.train = optimizer.apply_gradients(zip(grad, tvars), global_step=global_step, name='apply_grads')
        
        tf.summary.scalar('loss', self.loss)
        tf.summary.scalar('learning_rate', lr)
        tf.summary.scalar('grad_norm', grad_norm)
    

    def fill_feed_dict(self, input_word, text_length, true_label, is_training=False):
        feed_dict = {
            self.input_word: input_word,
            self.seq_length: text_length,
            self.true_label: true_label,
            self.is_training: is_training
        }
        return feed_dict